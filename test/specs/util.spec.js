var util = require('../../dist/utils.js');
var assert = require('assert');
var jsonfile = require('jsonfile');
var _ = require('lodash');

var player1 = {
    name: 'carrick rangers fc',
    values:
    [
        { value: 8.84, page: 'cloudbet' },
        { value: 8.13, page: 'betcoin' }
    ]
};
var player2 = {
    name: 'ballinamallard united fc',
    values:
    [
        { value: 1.4, page: 'cloudbet' },
        { value: 1.47, page: 'betcoin' }
    ]
};
var draw = {};

describe('Array', function() {
  describe('Utils::arbitrageCalculator', function() {
    it('should calculate for two players', function() {
        var results = util.arbitrageCalculator(player1, player2);
        assert.deepEqual(results,
            {
                player1: { page: 'cloudbet', name: 'carrick rangers fc' },
                player2: { page: 'betcoin', name: 'ballinamallard united fc' },
                sum: 0.7933942807892388
            });
    });
  });

    describe('Utils::normaliseName', function() {
        it('should calculate for dos players', function() {
        var results = util.normaliseName('Real Sociedad FC', [{ name: 'Malaga CF' }, { name: 'real sociedad u20' }, { name: 'draw' }]);
        assert.equal(results, 'real sociedad u20');
        });

        it('should calculate for dos players', function() {
          var results = util.normaliseName('Manchester United', [{ name: 'MAN.UD.' }, { name: 'MAN.CT.' }, { name: 'draw' }]);
          assert.equal(results, 'MAN.UD.');
        });
    });
});

describe('Page', function() {
    var file = __dirname + '/../resources/page1.json';
    var page = jsonfile.readFileSync(file);

    describe('retrieve games from page', function() {
        it('should extract the Games from a page', function() {
            var results = util.extractGames(page);
            assert.deepEqual(_.size(results), 40);
        });
        it('should retrieve the games from a page', function() {
            var results = util.getGames(page);
            assert.deepEqual(_.size(results), 2);
        });
    });
});

describe('get names', function() {
    var vitoria = 'vitoria',
        vitoriaBa = 'vitoria ba',
        daConquistaBa = 'vitoria da conquista ba',
        daConquista = 'vitoria da conquista';

    describe('retrieve games from page', function() {
        it('should be true for the same string', function() {
            assert.equal(util.checkNamesMatch(vitoria, vitoria), true);
        });
        it('should be true for almost the same string', function() {
            assert.equal(util.checkNamesMatch(daConquista, daConquistaBa), true);
            assert.equal(util.checkNamesMatch(vitoria, vitoriaBa), true);
        });
        it('should be false for strings that require a significant number of changes', function() {
            assert.equal(util.checkNamesMatch(vitoria, daConquistaBa), false);
        });
    });
});
