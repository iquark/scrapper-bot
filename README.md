To start the server

    $ gulp
    
To run a scraper, for example cloudbet:

    $ gulp casper --page cloudbet
    
In case we want to know what is going on, with the optional parameter *verbose*:

    $ gulp casper --verbose --page cloudbet

To run the tests:

    $ gulp test
    