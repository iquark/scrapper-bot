var gulp = require('gulp'),
    mocha = require('gulp-mocha');
    nodemon = require('gulp-nodemon'),
    babel = require('gulp-babel'),
    eslint = require('gulp-eslint'),
    del = require('del')
	argv = require('yargs').argv,
    casperJs = require('gulp-casperjs');

gulp.task('lint', function() {
    return gulp.src(['!dist/**/*.js', '!node_modules/**', 'src/**/*.js'])
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

gulp.task('copy', ['clean'], function () {
    return gulp
        .src('src/templates/*.dust')
        .pipe(gulp.dest('dist/templates'));
});

gulp.task('clean', function() {
    return del([
        'dist/**/*', 'dist'
    ]);
});

gulp.task('babel', ['clean'], function() {
    return gulp.src('src/**/*.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(gulp.dest('dist'));
});

gulp.task('default', ['copy', 'babel'], function() {
	nodemon({
		script: 'dist/index.js',
		ext: 'js dust css',
		env: {'NODE_ENV': 'development' },
        tasks: ['copy', 'babel'],
        ignore: ['dist']
	});
});


gulp.task('casper', ['copy', 'babel'], function() {
	if (!!argv.page) {

        console.log('executing ', argv.page);
        var options = { binPath: './node_modules/casperjs/bin/casperjs', command: '' };

        if (!!argv.verbose) {
            options.command = '--verbose';
        }

        gulp.src(`dist/bots/${argv.page}.js`)
            .pipe(casperJs(options));
    }
});

gulp.task('test', ['copy', 'babel'], function() {
    gulp.src('test/specs/*.js', {read: false})
        // gulp-mocha needs filepaths so you can't have any plugins before it
        .pipe(mocha({reporter: 'nyan'}))
});
//gulp.task('default', ['clean', 'babel', 'copy', 'start']);
