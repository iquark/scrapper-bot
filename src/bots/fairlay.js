import {
    dump, convertToDecimal
}
    from '../utils';

var casper = require('casper').create({
    verbose: false,
    logLevel: 'debug',
    loadImages: false,
    loadPlugins: false,
    waitTimeout: 35000,
    viewportSize: {
        width: 1160,
        height: 1500
    }
});
const PAGE = 'fairlay';
const sportSelector = '.categories-filters a.btn-primary';
const gameSelector = '.markets .table tbody tr';

var sports = {};
var page = {
    get: 'results',
    type: 'page',
    name: PAGE,
    sports: {}
};
var sportIdx = 2; // there is a header
var currentPage = 1;

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
    if (
        /doubleclick.net|googleadservices.com|twitter.com|facebook.com|.png|.woff/
            .test(requestData.url)) {
        this.log('Aborting ' + requestData.url, 'warning');
        request.abort();
    }
};

casper.on('resource.requested', onResourceRequested);

casper.getGame = function (sport) {
    const games = this.getElementsInfo(gameSelector),
        length = games.length;

    for (let idx = 0; idx < length; idx++) {
        let cells = this.getCells(games[idx].html);
        let firstCell = cells[0];
        const rowspan = firstCell.getAttribute('rowspan');
        let indexOdd = 3;

        if (!!rowspan) {
            try {
                let game = {
                    type: 'game',
                    id: firstCell.innerText.trim(),
                    players: [],
                    path: '',
                    odds: [],
                    meta: {
                        page: page.name,
                        sport: sport.name
                    }
                };
                let odd = {
                    type: 'odd',
                    frozen: false,
                    name: 'Moneyline',
                    values: [],
                    meta: {
                        page: PAGE,
                        sport: sport.id,
                        game: game.id
                    }
                };

                let limit = idx + parseInt(rowspan, 10);
                firstCell = cells[1];

                do {
                    if (game.players.length < 2) {
                        game.players.push({name: firstCell.innerText.trim()});
                    }
                    const oddInfo = cells[indexOdd].innerText;

                    odd.values.push({
                        name: firstCell.innerText.trim(),
                        value: casper.evaluate(convertToDecimal, oddInfo.trim()),
                        frozen: false
                    });

                    if (++idx < limit) {
                        cells = this.getCells(games[idx].html);
                        firstCell = cells[0];
                        indexOdd = 2;
                    }

                } while (idx < limit);

                console.log(JSON.stringify(game));
                console.log(JSON.stringify(odd));
                idx = limit - 1;
            } catch (e) {
                this.log(`Error: ${e.message}.`, 'error');
            }
        }

    }

    try {
        return this.nextPage(sport);
    } catch (exception) {
        this.log('Exception: ' + exception.message, 'error');
    }
};

casper.isNextPaginationDisabled = function () {
    const paginator = this.getElementInfo('.pagination');
    let paginatorDOM = document.createElement('html');
    paginatorDOM.innerHTML = paginator.html;

    const activePage = paginatorDOM.querySelector('.active');
    let sibling = activePage.nextElementSibling;

    return sibling.className === 'disabled';
};

casper.nextPage = function (sport) {
    if (this.exists('.pagination')) {
        const baseUrl = this.getCurrentUrl().split('?')[0];
        if (!this.isNextPaginationDisabled()) {
            return this.thenOpen(baseUrl.concat(`?page=${ ++currentPage }`),
                function then() {
                    return this.getGame(sport);
                }, function onTimeout() {
                    return this.getNextSport();
                });
        }
    }
    return;
};

casper.getCells = function (html) {
    const table = document.createElement('table');
    table.innerHTML = html;
    const tr = table.querySelector('tr');
    return tr.querySelectorAll('td');
};

// Get the sport, its name and create the space for it
casper.getSport = function () {
    const selector = sportSelector;
    try {
        if (this.exists(selector)) {
            let sportName = this.getElementInfo(selector).text.trim(),
                sportId = sportName.toLowerCase();

            if (!sports.hasOwnProperty(sportId)) {
                sports[sportId] = {
                    type: 'sport',
                    path: '',
                    id: sportId,
                    name: sportName,
                    games: {},
                    meta: {
                        page: PAGE
                    }
                };
                this.echo(JSON.stringify(sports[sportId]));
            }
            return this.getGame(sports[sportId]);
        } else {
            page.sports = sports;
            this.exit();
        }
    } catch (e) {
        this.log(e.message, "error");
        this.log(e.stack, "debug");
    }

    return;
};
casper.getNextSport = function () {
    const selector = sportSelector;
    let sportName = this.getElementInfo(selector).text;
    sportIdx++;
    let next = this.evaluate((selector) => document.querySelector(selector)
        .nextElementSibling.href, selector);

    return this.thenOpen(next).waitFor(() => {
        return this.getElementInfo(sportSelector).text !==
            sportName;
    }, function pass() {
        return this.getSport();
    }, function fail() {
        this.exit();
    });
};
// Open the live betting page
casper.start('https://www.fairlay.com/category/soccer/all/moneyline/')
    .then(function () {
        // Access the internal iFrame
        this.log('====> Here starts', 'debug');
        // open the full list
    })
    .waitForSelector(sportSelector)
    .then(function () {
        this.echo(JSON.stringify(page));
        return this.getSport();
    })
    .run();
