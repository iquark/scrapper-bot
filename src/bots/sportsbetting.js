import {convertToDecimal, normaliseName} from '../utils';

var casper = require('casper').create({
    verbose: false,
    logLevel: 'debug',
    loadImages: false,
    loadPlugins: false,
    waitTimeout: 25000,
    viewportSize: {width: 1160, height: 1500},
    onStepTimeout,
});

const PAGE = 'sportsbetting';
const sportSelector = '#active > ul.sport > li.sport';
const leagueSelector = 'ul.league li.league';
const gameSelector = 'ul.game li.game';

var sports = {};
var page = {
    get: 'results',
    type: 'page',
    name: PAGE,
    sports: {}
};
var sportIdx = 1;
var leagueIdx = 1;
var gameIdx = 1;

function onStepTimeout(self, m) {
    this.echo(JSON.stringify({err_code: 504, message: `Timeout step ${m}`}));
}

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
    if (/doubleclick.net|googleadservices.com|twitter.com|facebook.com|.png|.woff/.test(requestData.url)) {
        this.log('Aborting ' + requestData.url, 'warning');
        request.abort();
    }
};

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

casper.on('error', function (msg, backtrace) {
    this.log('Error: ' + JSON.stringify(msg), 'error');
    this.log('Error: ' + JSON.stringify(backtrace), 'error');
    casper.exit();
    throw new ErrorFunc("fatal", "error", "filename", backtrace, msg);
});

casper.on('resource.requested', onResourceRequested);

casper.normaliseName = normaliseName;

// get each odds from a given game
casper.getOdds = function (game) {;
    const oddSelector = '#gvPropContainer .gvProp';
    const match = `#gvGameFixed[brmatchid="${game.meta.matchId}"]` + ( Object.keys(sports).length === 1 ? ', #gvGameFixed[brmatchid="0"]' : '' );

    this.waitForSelector(match, function () {
        try {
            if (this.exists(oddSelector)) {
                const allOdds = this.getElementsInfo(oddSelector);
                const oddsLength = allOdds.length;

                for (var idx = 0; idx < oddsLength; idx++) {
                    let odd = document.createElement('html');

                    odd.innerHTML = allOdds[idx].html;
                    const data = odd.querySelectorAll('ul li');
                    const oddName = odd.querySelector('.propTitle').textContent;
                    const frozen = !!odd.querySelector('.propStatus.frozen');

                    let oddInfo = {
                        name: oddName,
                        type: 'odd',
                        frozen: frozen,
                        values: [],
                        meta: {
                            page: game.meta.page,
                            sport: game.meta.sport,
                            game: game.id
                        }
                    };

                    for (var lidx = 0; lidx < data.length; lidx++) {
                        let value = data[lidx].querySelector('.odds').textContent;
                        let name = data[lidx].querySelector('.name').textContent.toLowerCase() || '';

                        oddInfo.values.push({
                            value: casper.evaluate(convertToDecimal, value),
                            name: /moneyline/i.test(oddName) ? casper.normaliseName(name, game.players) : name
                        });
                    }

                    console.log(JSON.stringify(oddInfo));
                    sports[game.meta.sport.toLowerCase()].games[game.id].odds.push(oddInfo);
                }
            } else {
                this.log('do not exists ' + oddSelector, 'error');
            }
        } catch (e) {
            this.log('getOdds:' + e.message, 'error');
        }
        gameIdx++;

        return this.getGame(game.meta.sport);
    });
};

// Get the odds from each game
casper.getGame = function (sport) {
    const selector = sportSelector + ':nth-child(' + sportIdx + ') ' + leagueSelector + ':nth-child(' + leagueIdx + ') ' + gameSelector + ':nth-child(' + gameIdx + ')';

    try {
        const exists = this.exists(selector);

        if (!sport) {
            this.log('Sport is undefined!', 'error');
        }

        if (exists) {
            const players = this.getElementInfo(selector + ' .gameTitle').text.toLowerCase().split(/\s(vs\.?|at)\s/);
            let gameId = this.getElementAttribute(selector, 'gameid');
            let matchId = !!this.getElementInfo(selector).attributes.brmatchid ? this.getElementInfo(selector).attributes.brmatchid : gameId;

            sports[sport.toLowerCase()].games[gameId] = {
                type: 'game',
                id: gameId,
                path: '',
                players: [{
                    name: players[0]
                }, {
                    name: players.length > 2 ? players[2] : players[1]
                }],
                odds: [],
                meta: {
                    page: sports[sport.toLowerCase()].meta.page,
                    sport: sport,
                    matchId: matchId
                }
            };

            this.echo(JSON.stringify(sports[sport.toLowerCase()].games[gameId]));
            this.click(selector);

            const match = `#gvGameFixed[brmatchid="${matchId}"]` + ( Object.keys(sports).length === 1 ? ', #gvGameFixed[brmatchid="0"]' : '' );

            return this.waitForSelector('#gvGame').getOdds(sports[sport.toLowerCase()].games[gameId], sports[sport.toLowerCase()]);
        } else {
            leagueIdx++;
            gameIdx = 1;

            return this.getLeague(sport);
        }
    } catch (e) {
        this.log('getGame:' + e.message + '\n' + e, 'error');
    }
};

// Get every match from every league
casper.getLeague = function (sport) {
    const selector = sportSelector + ':nth-child(' + sportIdx + ') ' + leagueSelector + ':nth-child(' + leagueIdx + ')';

    try {
        if (this.exists(selector)) {
            return this.getGame(sport);
        } else {
            // if there are no more leagues, jump to the next sport
            sportIdx++;
            gameIdx = 1;
            leagueIdx = 1;
            return this.getSport();
        }
    } catch (e) {
        this.echo(JSON.stringify({err_code: 422, message: e.message}));
    }
    return;

};

// Get the sport, its name and create the space for it
casper.getSport = function () {
    const selector = sportSelector + ':nth-child(' + sportIdx + ')';

    try {
        if (this.exists(selector)) {
            let sportId = this.getElementInfo(selector).attributes.sportid;
            let sportName = this.getElementInfo(selector + ' .sportName').text;

            if (!sports.hasOwnProperty(sportName.toLowerCase())) {
                sports[sportName.toLowerCase()] = {
                    type: 'sport',
                    path: '',
                    id: sportId,
                    name: sportName,
                    games: {},
                    meta: {
                        page: PAGE
                    }
                };
            }

            this.echo(JSON.stringify(sports[sportName.toLowerCase()]));

            return this.getLeague(sportName);
        } else {
            page.sports = sports;
            this.exit();
        }
    } catch (e) {
        this.echo(JSON.stringify({err_code: 422, message: e.message}));
    }
    return;
};


// Open the live betting page
casper.start('https://www.sportsbetting.ag/live-betting')
    .then(function () {
        // Access the internal iFrame
        this.page.switchToChildFrame(0);
        this.log('====> Here starts: Clicking on #btnGameView exists: ' + this.exists('#btnGameView'), 'debug');
        // open the full list
        this.click('#btnGameView');
    })
    .waitForSelector('.gvGameListContainer')
    .then(function () {
        this.echo(JSON.stringify(page));
        return this.getSport();
    })
    .run();
