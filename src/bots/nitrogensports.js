import {
    dump, convertToDecimal
}
    from '../utils';

var casper = require('casper').create({
    verbose: false,
    logLevel: 'debug',
    loadImages: false,
    loadPlugins: false,
    waitTimeout: 35000,
    viewportSize: {
        width: 1160,
        height: 1500
    }
});
const PAGE = 'nitrogensports';

var sports = {};
var page = {
    get: 'results',
    type: 'page',
    name: PAGE,
    sports: {}
};
var games = [];

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
    if (
        /doubleclick.net|googleadservices.com|twitter.com|facebook.com|youtube.com|.png|.woff/
            .test(requestData.url)) {
        this.log('Aborting ' + requestData.url, 'warning');
        request.abort();
    }
};

casper.on('resource.requested', onResourceRequested);

casper.convertToDOMObject = function (html) {
    const root = document.createElement('html');
    root.innerHTML = html
    return root;
};

// Gets the game info and its odds
casper.getGameAndOdds = function (events, sport) {
    const length = events.length;
    let exists = false;
    let players = [];
    let values = [];

    for (let idx = 0; idx < length; idx++) {
        const event = events[idx];
        const selected = event.querySelector('select option[selected="selected"]');

        if (selected && /^ML/.test(selected.innerText)) {
            event.querySelector('.event-participant > .period-limit').remove();
            const participant = event.querySelector('.event-participant').innerText;

            exists = true;
            players.push(participant);
            values.push({
                name: participant,
                value: casper.evaluate(convertToDecimal, selected.innerText.split(' ')[1]),
                frozen: false
            });
        }
    }

    if (exists) {
        let game = {
            type: 'game',
            id: players.join(' vs '),
            players: [{name: players[0]}, {name: players[1]}],
            path: '',
            odds: [],
            meta: {
                page: page.name,
                sport: sport.name
            }
        };
        this.echo(JSON.stringify(game));

        this.echo(JSON.stringify({
            type: 'odd',
            frozen: false,
            name: 'Moneyline',
            values,
            meta: {
                page: PAGE,
                sport: sport.id,
                game: game.id
            }
        }));
    }
};

casper.getGames = function (sport) {
    const selector = '.events-result-set .event';

    try {
        if (this.exists(selector)) {
            const events = this.getElementsInfo(selector);
            const length = events.length;

            for (let idx = 0; idx < length; idx++) {
                let event = this.convertToDOMObject(events[idx].html);
                let eventRows = event.querySelectorAll('.event-row');

                this.getGameAndOdds(eventRows, sport);
            }
        }
        return this.getLeague();
    } catch (e) {
        this.log(e.message, "error");
        this.log(e.stack, "debug");
    }

};

// Get the sport, its name and create the space for it
casper.getSport = function () {
    const selector = '#page-find-games .page-title';

    try {
        if (this.exists(selector)) {
            let sportName = this.getElementInfo(selector).text;

            if (!sports.hasOwnProperty(sportName)) {
                sports[sportName] = {
                    type: 'sport',
                    path: '',
                    id: sportName,
                    name: sportName,
                    games: {},
                    meta: {
                        page: PAGE
                    }
                };
                this.echo(JSON.stringify(sports[sportName]));
            }
            return this.getGames(sports[sportName]);
        } else {
            return this.getLeague();
        }
    } catch (e) {
        this.log(e.message, "error");
        this.log(e.stack, "debug");
    }

    return;
};

var leagueIdx = 1;
casper.getLeague = function () {
    let selector = 'li.menu-item-sport.dropdown.open .menu-item-league';
    let nextLeague = `${selector}:nth-child(${leagueIdx}) a`;

    if (this.exists(nextLeague)) {
        leagueIdx++;

        let league = this.getElementInfo(nextLeague).text;

        this.click(nextLeague);

        return this.waitForSelector('.events-result-set', function () {
            return this.getSport();
        }, function (exc) {
            return this.getLeague();
        });
    }

    this.exit();

    return;
};

var iconCurrent = 'icon-star';

function waitAndGetLeague() {
    // Access the internal iFrame
    // open the full list
    this.echo(JSON.stringify(page));

    this.click(`.${iconCurrent}`);
    this.waitForSelector('li.menu-item-sport.dropdown.open')
        .then(function () {
            this.getLeague();
        });
}

// Open the live betting page
casper.start('https://nitrogensports.eu')
    .waitForSelector('#modal-welcome-new-button', function then() {
        this.log('modal welcome found', 'info');
        this.click('#modal-welcome-new-button');
        this.waitForSelector(`.${iconCurrent}`, waitAndGetLeague, function timeout() {
            this.waitForSelector('#modal-welcome-new-button', function then() {
                // try again
                this.click('#modal-welcome-new-button');
                this.waitForSelector(`.${iconCurrent}`, waitAndGetLeague, function timeout() {
                });
            }, function timeout() {
            });
        });
    })
    .run();
