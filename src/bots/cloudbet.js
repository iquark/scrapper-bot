import {dump, convertToDecimal} from '../utils';

var casper = require('casper').create({
    verbose: false,
    logLevel: 'debug',
    waitTimeout: 25000,
    viewportSize: {width: 1060, height: 1000},
    onStepTimeout,
});

const host = 'https://www.cloudbet.com';
const PAGE = 'cloudbet';
const sportSelector = '.nav-sports-item';
const gameSelector = 'ul li';
const marketSelector = '.market-list';
const eventTitleSelector = '.main-content .breadcrumb .breadcrumb__item:last-child';

var page = {
    type: "page",
    name: PAGE,
    host,
    sports: {},
};

var gameIdx = 1;
var sportIdx = 1;
var oddIdx = 1;

function onStepTimeout(self, m) {
    this.echo(JSON.stringify({err_code: 504, message: `Timeout step ${m}`}));
}

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
    if (
        /doubleclick.net|googleadservices.com|twitter.com|facebook.com|youtube.com|.png|.woff/
            .test(requestData.url)) {
        this.log('Aborting ' + requestData.url, 'warning');
        request.abort();
    }
};

casper.on('resource.requested', onResourceRequested);

casper.on('error', function (msg, backtrace) {
    this.log(`Error: ${JSON.stringify(msg)}`, 'error');
    this.log(`Error: ${JSON.stringify(backtrace)}`, 'error');
    casper.exit();
    throw new ErrorFunc("fatal", "error", "filename", backtrace, msg);
});

casper.clickWhileSelector = function (selector, idx) {
    return this.then(function () {
        const i = idx || 1;
        const selectorNth = selector.replace('$idx', i);

        try {
            // the first one to three odds will be opened, it tries with the first five
            if (this.exists(selectorNth) || i < 6) {
                if (this.exists(selectorNth + ' .icon-angle-down')) {
                    this.click(selectorNth);
                    return this.waitForSelector(`${selectorNth} .icon-angle-up`).clickWhileSelector(selector, i + 1);
                } else {
                    return this.clickWhileSelector(selector, i + 1);
                }
            }
        } catch (e) {
            this.log('clickWhileSelector: Error: ' + e.message, 'error');
        }
        return;
    });
};

casper.getOdd = function (sport, game) {
    const selector = marketSelector + ' .market-list__item:nth-child(' + oddIdx + ')';

    if (this.exists(selector)) {
        const oddName = this.getElementInfo(selector + ' .market-list__item__title').text;
        let i = 1,
            selectorNth = selector + ' .col:nth-child(' + i + ')',
            odd = {
                type: 'odd',
                frozen: false,
                name: oddName,
                values: [],
                meta: {
                    page: PAGE,
                    sport: sport.id,
                    game: game.id
                }
            };

        while (this.exists(selectorNth)) {
            let name = this.getElementInfo(selectorNth + ' .name').text || '';
            let values = {
                name: name.toLowerCase(),
                value: casper.evaluate(convertToDecimal, this.getElementInfo(selectorNth + ' .odds').text),
                frozen: false
            };

            if (this.exists(selectorNth + ' .odds .icon-lock')) {
                values.frozen = true;
            }

            odd.values.push(values);
            selectorNth = selector + ' .col:nth-child(' + (++i) + ')';
        }

        this.echo(JSON.stringify(odd));

        oddIdx++;
        return this.getOddsLoop(sport, game);
    } else {
        gameIdx++;
        return this.getGame(sport);
    }

};

casper.getOddLoop = function (sport, game, selector) {
    const oddName = this.getElementInfo(selector + ' .market-list__item__title').text;
    let i = 1,
        selectorNth = selector + ' .col:nth-child(' + i + ')',
        odd = {
            type: 'odd',
            frozen: false,
            name: oddName,
            values: [],
            meta: {
                page: PAGE,
                sport: sport.id,
                game: game.id
            }
        };

    if (this.exists(selectorNth)) {
        let values = {
            name: this.getElementInfo(selectorNth + ' .name').text,
            value: casper.evaluate(convertToDecimal, this.getElementInfo(selectorNth + ' .odds').text),
            frozen: false
        };

        if (this.exists(selectorNth + ' .odds .icon-lock')) {
            values.frozen = true;
        }

        odd.values.push(values);
    }

    this.echo(JSON.stringify(odd));
    oddIdx++;

    return this.getOddsLoop(sport, game);
};

casper.getOddsLoop = function (sport, game, idx) {
    const selector = `.market-list__item:nth-child(${oddIdx})`;

    if (this.exists(selector)) {
        if (this.exists(`${selector} a .icon-angle-down`)) {
            this.click(`${selector}  a`);
            return this.waitForSelector(`${selector} a .icon-angle-up`).getOdd(sport, game, selector);
        } else {
            return this.getOdd(sport, game, selector);
        }
    } else {
        oddIdx = 1;
        gameIdx++;
        return this.getGame(sport);
    }
};

casper.getOdds = function (sport, game) {
    if (this.exists(marketSelector)) {
        const noMarketExists = this.exists(marketSelector + ' .no-markets');

        // if there are no markets, go to next game
        if (noMarketExists) {
            gameIdx++;
            return this.getGame(sport);
        }

        oddIdx = 1;
        return this.getOddsLoop(sport, game);
    } else {
        gameIdx++;
        return this.getGame(sport);
    }

};

casper.getGame = function (sport) {
    const selector = sportSelector + ':nth-child(' + sportIdx + ') ' + gameSelector + ':nth-child(' + gameIdx + ')'

    if (this.exists(selector) && this.exists(selector + ' .event-name')) {
        const players = this.getElementInfo(selector + ' .event-name').text.split(' v '),
            link = this.getElementInfo(selector + ' a'),
            id = link.attributes.href.split('/')[link.attributes.href.split('/').length - 1],
            game = {
                type: 'game',
                id: players.join(' vs '),
                players: [{name: players[0]}, {name: players[1]}],
                path: link.attributes.href,
                odds: [],
                meta: {
                    page: page.name,
                    sport: sport.name
                }
            };

        this.echo(JSON.stringify(game));

        return this.thenOpen(host + game.path).waitForUrl(host + game.path, function then() {
            return this.waitFor(function () {
                const selectorSp = sportSelector + ':nth-child(' + sportIdx + ')';
                this.click(selectorSp);

                let result = this.evaluate((players, eventTitleSelector) => {
                    const title = document.querySelector(eventTitleSelector).innerText.toLowerCase(),
                        names = players.join(' v ').toLowerCase();
                    console.log(title, '===', names);
                    return title === names;
                }, players, eventTitleSelector);

                return result;
            }, function then() {
                this.getOdds(sport, game);
            }, function timeout() {
                sportIdx++;
                return this.getSport();
            });
        }, function timeout() {
            sportIdx++;
            return this.getSport();
        });
    } else {
        sportIdx++;
        return this.getSport();
    }
};

casper.getSport = function () {
    const selector = sportSelector + ':nth-child(' + sportIdx + ')';

    if (this.exists(selector)) {
        const sportName = this.getElementInfo(selector + ' .sports-name').text;
        const id = sportName.toLowerCase();

        const sport = {
            type: 'sport',
            name: sportName,
            id: id,
            path: '',
            games: {},
            meta: {
                page: page.name
            }
        };

        this.echo(JSON.stringify(sport));
        gameIdx = 1;
        this.click(selector);

        return this.waitForSelector(selector + ' .icon-angle-up',
            function then() {
                return this.getGame(sport)
            }, function timeout() {
                sportIdx++;
                return this.getSport();
            });
    } else {
        this.exit();
    }
};

casper.getPage = function () {
    this.echo(JSON.stringify(page));
    sportIdx = 1;
    return this.getSport();
};

casper.start('https://www.cloudbet.com/en/sports/live')
    .then(function () {
        this.log('\n\t\tSTARTED THE PAGE CLOUDBET', 'debug');
    })
    .waitForSelector('.recaptcha-checkbox-checkmark, #cmpLiveSportsNavMenu')
    .then(function () {
        if (this.exists('.recaptcha-checkbox-checkmark')) {
            this.click('.recaptcha-checkbox-checkmark');
        }
        this.waitForSelector('#cmpLiveSportsNavMenu')
            .then(function () {
                this.getPage();
            });
    })
    .run();
