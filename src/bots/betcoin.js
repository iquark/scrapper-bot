/**
https://sports.betcoin.ag/#/sport/live
.rightNowResults
  .events
    div:nth-child(x)
      sportName = h4.innerText
      .eventstable
        tbody:nth-child(y)
          eventName = header a.innerText
            click(a)
              whaitForSelector('.liveEvents')

 **/


import { dump, convertToDecimal } from '../utils';

var casper = require('casper').create({
  verbose: false,
  logLevel: 'debug',
  loadImages: false,
  loadPlugins: false,
  waitTimeout: 35000,
  viewportSize: {width: 1160, height:1500}
});
const PAGE = 'betcoin';
const sportSelector = '.rightNowResults .events > div';
const gameSelector = '.eventstable tbody';

var sports = {};
var page = {
  get: 'results',
  type: 'page',
  name: PAGE,
  sports: {}
};
var sportIdx = 2; // there is a header
var gameIdx = 1;

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
  if (/doubleclick.net|googleadservices.com|twitter.com|facebook.com|youtube.com|.png|.woff/.test(requestData.url)) {
    this.log('Aborting '+requestData.url, 'warning');
    request.abort();
  }
};

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

casper.on('error', function(msg,backtrace) {
  this.log('Error: '+JSON.stringify(msg), 'error');
  this.log('Error: '+JSON.stringify(backtrace), 'error');
  casper.exit();
  throw new ErrorFunc("fatal","error","filename",backtrace,msg);
});

casper.on('resource.requested', onResourceRequested);
// get each odds from a given game
casper.getOdds = function(game) {
  const oddSelector = '.BettingContent[data-ng-if="match"] .events.in-play > div';

  try {
    let i = 2; // there is a header
    while(this.exists(`${oddSelector}:nth-child(${i})`)) {
      let selector = `${oddSelector}:nth-child(${i})`;

      if (this.exists(selector)) {
          const allOdds = this.getElementsInfo(oddSelector);
          const oddsLength = allOdds.length;

          for (let idx = 0; idx < oddsLength; idx++) {
            let odd = document.createElement('html');
            odd.innerHTML = allOdds[idx].html;
            const data = odd.querySelectorAll('.event-wrapper [class^="odd_team"]'); //[class^="col-"]
            const title = odd.querySelector('h4');
            if (!!title) {
              const oddName = title.textContent.trim();
              const frozen = false;

              let oddInfo = {
                name: oddName,
                type: 'odd',
                frozen: frozen,
                values: [],
                meta: {
                  page: game.meta.page,
                  sport: game.meta.sport,
                  game: game.id
                }
              };

              for (var lidx = 0; lidx < data.length; lidx++) {
                let value = data[lidx].querySelector('.coefficient').textContent,
                    name = data[lidx].title || '';

                oddInfo.values.push({
                  value: this.evaluate(convertToDecimal, value),
                  name: name.toLowerCase()
                });
              }

              console.log(JSON.stringify(oddInfo));
              sports[game.meta.sport.toLowerCase()].games[game.id].odds.push(oddInfo);
            }
          }
          break;
      } else {
        this.log('do not exists ' + selector, 'error');
      }
      i++;
    }
  } catch (e) {
    this.log('getOdds:'+e.message+' stack '+JSON.stringify(e), 'error');
  }
  gameIdx++;
  return this.getGame(game.meta.sport);
};

// Get the odds from each game
casper.getGame = function(sport) {
  const selector = `${sportSelector}:nth-child(${sportIdx}) ${gameSelector}:nth-child(${gameIdx}) header a`;

  try {
    const exists = this.exists(selector);

    if (this.exists(selector)) {
      const link = this.getElementInfo(selector);
      const players = link.text.toLowerCase().split(' - ');

      this.click(selector);

      return this.waitForSelector('.BettingContent[data-ng-if="match"]').then(function() {
        let game = {
          type: 'game',
          id: link.attributes.href.split('/')[link.attributes.href.split('/').length - 1],
          path: link.attributes.href,
          players: [{
            name: players[0]
          },{
            name: players[1]
          }],
          odds: [],
          meta: {
            page: sports[sport.toLowerCase()].meta.page,
            sport: sport,
            matchId: ''
          }
        };

        sports[game.meta.sport.toLowerCase()].games[game.id] = game;

        this.echo(JSON.stringify(game));
        gameIdx++;
        return this.waitForSelector('.match-teams, .eventsContainer .events.metro.in-play', function() {
          return this.waitFor(function() {
            let matchTeams;

            try {
              matchTeams = this.getElementInfo('.match-teams').text;
            } catch(exc) {
              this.log(exc.message, 'error');
              matchTeams = this.getElementInfo('.eventsContainer .events.metro header.ng-binding').text;
            }

            return !!matchTeams && (new RegExp(players[0], 'i')).test(matchTeams);
          }, function then() {
            return this.waitForSelector('.rightNowResults .events-collection').then(function() {
              return this.getOdds(game);
            });
          }, function timeout() {
            sportIdx++;
            gameIdx = 1;

            return this.waitForSelector('.rightNowResults .events-collection').then(function() {
              return this.getSport();
            });
          });
        }, function() {
          this.log('timeout: '+JSON.stringify(arguments), 'error');
        });
      });
    } else {
      sportIdx++;
      gameIdx = 1;

      return this.waitForSelector('.events-collection').then(function() {
        return this.getSport();
      });
    }
  }  catch (e) {
    this.log('\tgetGame:'+e.message+'\n'+e, 'error');
  }
};

// Get the sport, its name and create the space for it
casper.getSport = function() {
  const selector = `${sportSelector}:nth-child(${sportIdx})`;

  try {
    if (this.exists(selector+' h4')) {
      let sportName = this.getElementInfo(selector+' h4').text,
        sportId = sportName.toLowerCase();

      if (!sports.hasOwnProperty(sportName.toLowerCase())) {
        sports[sportName.toLowerCase()] = {
          type: 'sport',
          path: '',
          id: sportId,
          name: sportName,
          games: {},
          meta: {
            page: PAGE
          }
        };
      }

      this.echo(JSON.stringify(sports[sportName.toLowerCase()]));

      return this.getGame(sportName);
    } else {
      page.sports = sports;
      this.exit();
    }
  } catch(e) {
    this.echo(JSON.stringify({err_code: 422, message: e.message}));
  }

  return;
};


// Open the live betting page
casper.start('https://sports.betcoin.ag/#/sport/live')
.then(function() {
  // Access the internal iFrame
  this.log('====> Here starts: Clicking on .rightNowResults exists: '+this.exists('.events-collection'), 'debug');
  // open the full list
})
.waitForSelector('.events-collection')
.then(function() {
  this.echo(JSON.stringify(page));
  return this.getSport();
})
.run();
