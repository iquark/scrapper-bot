import {dump, convertToDecimal} from '../utils';

var casper = require('casper').create({
    verbose: false,
    logLevel: 'debug',
    loadImages: false,
    loadPlugins: false,
    waitTimeout: 55000,
    viewportSize: {width: 1160, height: 1500}
});
const PAGE = 'sportsbet';
const sportSelector = '.live-wrap .live-sport';
const gameSelector = '.live-event';

var sports = {};
var page = {
    get: 'results',
    type: 'page',
    name: PAGE,
    sports: {}
};
var sportIdx = 1;
var gameIdx = 2; // always start by 2 because there is a header above

// ignores all useless requests and data such as images to improve the load
function onResourceRequested(requestData, request) {
    if (/doubleclick.net|googleadservices.com|twitter.com|facebook.com|.png|.woff/.test(requestData.url)) {
        this.log('Aborting ' + requestData.url, 'warning');
        request.abort();
    }
};

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

casper.on('error', function (msg, backtrace) {
    this.log('Error: ' + JSON.stringify(msg), 'error');
    this.log('Error: ' + JSON.stringify(backtrace), 'error');
    casper.exit();
    throw new ErrorFunc("fatal", "error", "filename", backtrace, msg);
});

casper.on('resource.requested', onResourceRequested);

// get each odds from a given game
casper.getOdds = function (game) {
    const oddSelector = '.table.table-styled';
//.event-overlay
    return this.waitForSelector(oddSelector + ', .event-overlay', function pass() {
        if (!this.exists('.event-overlay')) {
            try {
                let i = 1;
                while (this.exists(`${oddSelector}:nth-child(${i})`) || i < 6) {
                    let selector = `${oddSelector}:nth-child(${i})`;

                    if (this.exists(selector)) {
                        const allOdds = this.getElementsInfo(oddSelector);
                        const oddsLength = allOdds.length;

                        for (var idx = 0; idx < oddsLength; idx++) {
                            let odd = document.createElement('html');

                            odd.innerHTML = allOdds[idx].html;
                            const data = odd.querySelectorAll('.cell');
                            const oddName = odd.querySelector('.title').textContent.trim();
                            const frozen = false;

                            let oddInfo = {
                                name: oddName,
                                type: 'odd',
                                frozen: frozen,
                                values: [],
                                meta: {
                                    page: game.meta.page,
                                    sport: game.meta.sport,
                                    game: game.id
                                }
                            };

                            for (var lidx = 0; lidx < data.length; lidx++) {
                                let value = data[lidx].querySelector('.odds').textContent,
                                    name = data[lidx].querySelector('.truncate').textContent || '';

                                oddInfo.values.push({
                                    value: this.evaluate(convertToDecimal, value),
                                    name: name.toLowerCase()
                                });
                            }

                            console.log(JSON.stringify(oddInfo));
                            sports[game.meta.sport.toLowerCase()].games[game.id].odds.push(oddInfo);
                        }
                    } else {
                        this.log('do not exists ' + selector, 'error');
                    }
                    i++;
                }
            } catch (e) {
                this.log('getOdds:' + e.message, 'error');
            }
        } else {
            this.log('Odds FROZEN');
        }

        gameIdx++;
        return this.back().waitForSelector('.fa-spinner', function pass() {
            return this.getGame(game.meta.sport);
        }, function fail() {
            return this.getGame(game.meta.sport);
        });
    }, function () {
        // odd Selector does NOT exist, going back and waiting for spinner
        gameIdx++;

        return this.back().waitForSelector('.fa-spinner', function pass() {
            return this.getGame(game.meta.sport);
        }, function fail() {
            return this.getGame(game.meta.sport);
        });
    });
};

// Get the odds from each game
casper.getGame = function (sport) {
    const selector = `${sportSelector}:nth-child(${sportIdx}) ${gameSelector}:nth-child(${gameIdx})`;

    return this.waitForSelector(`${sportSelector}:nth-child(${sportIdx})`).then(function () {
        try {
            const exists = this.exists(selector);

            if (!sport) {
                this.log('Sport is undefined!', 'error');
            }

            if (exists) {
                const players = this.getElementInfo(selector + ' .description b').text.toLowerCase().split(' - ')[0].split(' v ');
                let gameId = players.join(' v ');

                sports[sport.toLowerCase()].games[gameId] = {
                    type: 'game',
                    id: gameId,
                    path: '',
                    players: [{
                        name: players[0]
                    }, {
                        name: players[1]
                    }],
                    odds: [],
                    meta: {
                        page: sports[sport.toLowerCase()].meta.page,
                        sport: sport
                    }
                };

                // sending the game
                this.echo(JSON.stringify(sports[sport.toLowerCase()].games[gameId]));
                this.click(`${selector} .see-odds`);

                return this.waitForSelector('.score-holder', function pass() {
                    return this.getOdds(sports[sport.toLowerCase()].games[gameId]);
                }, function fail() {
                    this.log(`Page not loaded ${JSON.stringify(arguments)}`, 'error');
                });
            } else {
                sportIdx++;
                gameIdx = 2;

                return this.getSport();
            }
        } catch (e) {
            this.log('getGame:' + e.message + '\n' + e, 'error');
        }
    });
};

// get the sports once they are listed in the page
function getSport() {
    const selector = `${sportSelector}:nth-child(${sportIdx})`;

    try {
        if (this.exists(selector)) {
            let sportName = this.getElementInfo(selector + ' .live-sport-header .name').text.split(' - ')[0],
                sportId = sportName.toLowerCase();

            if (!sports.hasOwnProperty(sportName.toLowerCase())) {
                sports[sportName.toLowerCase()] = {
                    type: 'sport',
                    path: '',
                    id: sportId,
                    name: sportName,
                    games: {},
                    meta: {
                        page: PAGE
                    }
                };
            }

            this.echo(JSON.stringify(sports[sportName.toLowerCase()]));

            return this.getGame(sportName);
        } else {
            page.sports = sports;
            this.exit();
        }
    } catch (e) {
        this.echo(JSON.stringify({err_code: 422, message: e.message}));
    }

    return;
}

// Get the sport, its name and create the space for it, it waits the selector to appear
casper.getSport = function () {
    const selector = `${sportSelector}:nth-child(${sportIdx})`;

    this.waitForSelector(selector, getSport, getSport, 25000);
};


// Open the live betting page
casper.start('https://sportsbet.io/sports/live')
    .then(function () {
        // Access the internal iFrame
        this.log('====> Here starts: Clicking on .live-wrap exists: ' + this.exists('.live-wrap'), 'debug');
        // open the full list
    })
    .waitForSelector('.live-wrap', function () {
            this.echo(JSON.stringify(page));
            return this.getSport();
        }, function () {
            this.log('timeout: ' + JSON.stringify(arguments), 'error');
        }
    )
    .run();
