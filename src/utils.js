import _ from 'lodash';

/*
 * helped with:
 * - http://oddsconverter.co.uk
 * - http://www.betsmart.co/odds-conversion-formulas/
 */
export const convertToDecimal = function (odd) {
    let value = odd;

    if (typeof odd === 'string') {
        value = parseFloat(odd);
    }

    let valueAbs = Math.abs(value);
    // American: abs(number) >= 100, abs(number) <= 5000
    if (valueAbs >= 100 && valueAbs <= 5000) {
        if (value > 0) {
            value = (valueAbs / 100) + 1;
        } else {
            value = (100 / valueAbs) + 1;
        }
    }

    return value;
}

export const normaliseName = function (name, playerNames) {
    const length = playerNames.length;
    let result = name;
    let nameRE = new RegExp(name, 'i');
    let minimumDistance = 999999999;

    if (!/Draw/i.test(name)) {
        for (let idx = 0; idx < length; idx++) {
            let playerName = playerNames[idx].name;
            if (nameRE.test(playerName)) {
                return playerName;
            }

            let distance = levenshtein(name, playerName);
            if (distance < minimumDistance) {
                result = playerName;
                minimumDistance = distance;
            }
        }
    }

    return result;
};

/* arbitrage calculator
 * expects the players as:
 *   name: name of the player
 *   values:
 *     page: name of the page
 *     value: value of odd
 */
export const arbitrageCalculator = function (player1, player2, draw = null) {
    const value1 = Math.max.apply(null, _.map(player1.values, (item) => item.value));
    const value2 = Math.max.apply(null, _.map(player2.values, (item) => item.value));
    let value3;
    let messages = '';
    let arbitrage = {};
    let sum = 1 / value1 + 1 / value2;

    if (!!draw) {
        value3 = Math.max.apply(null, _.map(draw.values, (item) => item.value));
        sum += 1 / value3;
    }

    if (sum < 1) {
        const filter1 = _.filter(player1.values, {value: value1});
        const filter2 = _.filter(player2.values, {value: value2});
        const page1 = !!filter1 && !!filter1[0] ? filter1[0].page : 'page1';
        const page2 = !!filter2 && !!filter2[0] ? filter2[0].page : 'page2';

        messages += `bet on ${page1} for ${player1.name}`;
        messages += `, bet on ${page2} for ${player2.name}`;
        arbitrage = {player1: {page: page1, name: player1.name}, player2: {page: page2, name: player2.name}};
        if (!!draw) {
            const page3 = !!draw ? _.filter(draw.values, {value: value3})[0].page : 'none';
            messages += `, bet on ${page3} for ${draw.name}`;
            arbitrage.draw = {page: page3, name: draw};
        }
    }

    //console.log('\t messages: ' + messages + '\n\n');

    arbitrage.sum = sum;

    return arbitrage;
}

// returns the distance between two words
export const levenshtein = function (a, b) {
    if (a.length == 0) return b.length;
    if (b.length == 0) return a.length;

    // swap to save some memory O(min(a,b)) instead of O(a)
    if (a.length > b.length) {
        var tmp = a;
        a = b;
        b = tmp;
    }

    var row = [];
    // init the row
    for (var i = 0; i <= a.length; i++) {
        row[i] = i;
    }

    // fill in the rest
    for (var i = 1; i <= b.length; i++) {
        var prev = i;
        for (var j = 1; j <= a.length; j++) {
            var val;
            if (b.charAt(i - 1) == a.charAt(j - 1)) {
                val = row[j - 1]; // match
            } else {
                val = Math.min(row[j - 1] + 1, // substitution
                    prev + 1,     // insertion
                    row[j] + 1);  // deletion
            }
            row[j - 1] = prev;
            prev = val;
        }
        row[a.length] = prev;
    }

    return row[a.length];
}

export function getGames(page) {
    return processGames(extractGames(page));
}

export function extractGames(pages) {
    let gs = {};

    for (const p in pages) {
        for (const s in pages[p].sports) {
            for (const g in pages[p].sports[s].games) {
                let game = pages[p].sports[s].games[g],
                    names = getNames(game.players);

                if (!gs.hasOwnProperty(names.gameName)) {
                    gs[names.gameName] = Object.assign({}, game);
                    gs[names.gameName].odds = {};
                }

                for (const o of game.odds) {
                    let oddName = o.name;

                    if ((/Moneyline|Money Line/i).test(o.name)) {
                        oddName = 'moneyline';
                    } else if ((/^Which Team Will Win The Match|Match winner/i).test(o.name)) {
                        oddName = 'match winner';
                    } else if ((/^Match totals|^Total Games/i).test(o.name)) {
                        oddName = 'match totals';
                    }

                    if (!gs[names.gameName].odds[oddName]) {
                        gs[names.gameName].odds[oddName] = {};
                    }

                    if (oddName === 'match totals') {
                        try {
                            for (let index in o.values) {
                                if (/(^over|^under) (\d+(\.5)?)/.test(o.values[index].name)) {
                                    const [position, number] = o.values[index].name.split(' ');

                                    if (!gs[names.gameName].odds[oddName][number])
                                        gs[names.gameName].odds[oddName][number] = {};

                                    if (!gs[names.gameName].odds[oddName][number][position])
                                        gs[names.gameName].odds[oddName][number][position] = {};

                                    // game.matchtotals.'2.5'.under.betcoin = 3.45
                                    gs[names.gameName].odds[oddName][number][position][o.meta.page] = o.values[index].value;
                                }
                            }
                        } catch(exc) {
                            console.error('ERROR', exc.message, exc);
                        }
                    } else {
                        gs[names.gameName].odds[oddName][o.meta.page] = o;
                    }
                }
            }
        }
    }

    return gs;
}

const DRAW = 'draw';
export function processGames(all) {
    let result = {},
        playerNames = [];

    for (const name in all) {
        const game = all[name];
        for (let oddName in game.odds) {
            if ((/^Which Player Will Win The Match\?$|match winner/i).test(oddName.toLowerCase())) {
                oddName = 'match winner';
            }
            if ((/moneyline|match odds|^Wich|^Who|match winner/i).test(oddName.toLowerCase()) && _.size(game.odds[oddName]) > 1) {
                const names = getNames(game.players, playerNames);

                playerNames = _.union(playerNames, [names.gameName]);

                let pl1 = {name: names.player1, values: getValues(game.odds[oddName], names.player1)},
                    pl2 = {name: names.player2, values: getValues(game.odds[oddName], names.player2)},
                    draw = {name: DRAW, values: getValues(game.odds[oddName], DRAW)};

                if (!result[names.gameName]) {
                    result[names.gameName] = {...all[names.gameName], odds: {}};
                }

                result[names.gameName].odds[oddName] = {
                    pages: game.odds[oddName],
                    arbitrage: arbitrageCalculator(pl1, pl2, draw)
                };
            }

            // Over/Under
            if ((/match totals/i).test(oddName.toLowerCase()) && _.size(game.odds[oddName]) > 1) {
                const names = getNames(game.players, playerNames);

                playerNames = _.union(playerNames, [names.gameName]);

                for(let number in game.odds[oddName]) {
                    // game.odds[oddName][number]
                    // result[names.gameName].odds[oddName] = {
                    //     pages: game.odds[oddName][number].under,
                    //     arbitrage: arbitrageCalculator(pl1, pl2, draw)
                    // };
                }
            }
        }
    }

    return result;
}

export function getNames(players, playerNames = []) {
    let player1 = players[0].name.toLowerCase().trim(),
        player2 = players[1].name.toLowerCase().trim(),
        gameName = player1.localeCompare(player2) < 0 ? `${player1} vs ${player2}` : `${player2} vs ${player1}`,
        length = playerNames.length,
        [ name1, name2 ] = gameName.split(/\s+vs\s+/);

    for (let idx = 0 ; idx < length; idx++) {
        let [ stored1, stored2 ] = playerNames[idx].split(/\s+vs\s+/);

        if (checkNamesMatch(name1, stored1) || checkNamesMatch(name2, stored2)) {
            [ player1, player2 ] = [ stored1, stored2 ];
            gameName = playerNames[idx];
        }
    }

    return {
        player1,
        player2,
        gameName
    }
}

export function checkNamesMatch(player1, player2) {
    let name1 = player1.toLowerCase(),
        name2 = player2.toLowerCase(),
        threshold = Math.ceil(Math.sqrt(Math.max(name1.length, name2.length)));

    return name1 === name2 || levenshtein(name1, name2) <= threshold;
}

function getValues(odds, player) {
    return _.compact(_.map(odds, (odd) => (
        !!_.filter(odd.values, {name: player})[0] ?
            {value: (_.filter(odd.values, {name: player})[0]).value, page: odd.meta.page} : null
    )));
}
