"use strict";

let express = require('express');
let adaro = require('adaro');
let cluster = require('cluster');

let VERBOSE = true;
let server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
let server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

let worker = null,
    status = {scraper: 'Scraper stopped', history: []},
    pages = {},
    games = {},
    ongoing = {};

function debug(message) {
    if (VERBOSE) {
        console.info(message);
    }
}

/**
 * It starts a thread with the script that starts the scrapers on demand
 */
function startScraper() {
    status.scraper = 'Starting Scraper';
    worker = cluster.fork();
    debug(`Worker ${worker.id}`);
    status.scraper = 'Scraper started';

    worker.on('message', (msg) => {
        switch (msg.type) {
            case 'pages':
                pages = msg.pages;
                break;
            case 'games':
                games = msg.games;
                break;
            case 'finished':
                console.log(`${msg.page} has finished scraping`);

                if (ongoing[msg.page]) {
                    delete ongoing[msg.page];
                }

                break;
            default:
                break;
        }
    });
}

if (cluster.isMaster) {
    const app = express();

    // adaro
    const adaroOpts = {
        helpers: [
            // NOTE: function has to take dust as an argument.
            (dust) => {
                // eslint no-param-reassign
                dust.helpers.iter = (chunk, context, bodies, params) => {
                    const obj = dust.helpers.tap(params.obj, chunk, context);

                    const iterable = [];

                    for (const key in obj) {
                        if (obj.hasOwnProperty(key)) {
                            const value = obj[key];

                            iterable.push({
                                '$key': key,
                                '$value': value,
                                '$type': typeof value,
                            });
                        }
                    }

                    return chunk.section(iterable, context, bodies);
                },
                dust.filters.slug = (value) => {
                    if (typeof value === 'string') {
                        return value.replace(/\s|\./g, '-').toLowerCase();
                    }
                    return value;
                }
            },
            // So do installed modules
            'dustjs-helpers',
        ],
    };

    app.engine('dust', adaro.dust(adaroOpts));
    app.set('view engine', 'dust');
    app.set('views', `${__dirname}/templates`);

    // serving static files
    app.use(express.static(`${__dirname}/../public`));

    app.get('/', (req, res) => {
        let pageNames = ['cloudbet', 'betcoin', 'sportsbet', 'sportsbetting', 'fairlay', 'nitrogensports'];

        res.render('index', {worker, status, title: 'scrapers', pages, games, ongoing, pageNames});
    });

    app.listen(server_port, server_ip_address, () => {
        debug(`1.App listening on port ${server_port}!`);
        startScraper();
    });

    app.get('/start', (req, res) => {
        startScraper();
        res.redirect('/');
    });

    app.get('/list/:page', (req, res) => {
        if (!!req.params.page) {
            worker.send({page: req.params.page, get: 'list'});
            ongoing[req.params.page] = true;
            res.redirect('/');
        }
    });

    app.get('/odds/:page/:evtId', (req, res) => {
        if (!!req.params.page) {
            worker.send({page: req.params.page, get: req.params.evtId});
            page[req.params.page][req.params.evtId].odds = {fetching: true};
            res.redirect('/');
        }
    });

    cluster.on('exit', (worker, code, signal) => {
        debug(`worker PID: ${worker.process.pid}, CODE: ${code}, SIGNAL: ${signal} died`);
        startScraper();
    });
} else {
    debug('CHILD');
    require('./scraper');
}
