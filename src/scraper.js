"use strict";

import { getGames } from './utils';

console.log('Scraper started');

// sends data to the server
function sendData(data) {
  try {
    process.send(JSON.parse(data));
  } catch (e) {
    process.send({ err_code: 422, message: JSON.stringify(e) });
  }

}

// sends a message to the server
function sendMessage(message) {
  process.send({ message });
}

var pages = {};
var games = {};

/** formats the data arrived from the scrapers in the following way
 *  { pageName1: {
 *      sportName1: {
 *        gameId1: {
 *          odds: [
 *            {
 *              name: oddName1,
 *              values: [
 *                { name: team1, value: number },
 *                { name: team2, value: number },
 *                { name: draw, value: number },
 *              ]
 *            }
 *          ]
 *        },
 *        gameId2: ...
 *      },
 *      sportName2: ...
 *    },
 *    pageName2: ...
 *  }
 *
 */
function formatData(data) {
  let message = '',
    player1,
    player2;

  switch (data.type) {
    case 'page':
      message = `Received page ${data.name}`;
      pages[data.name] = data;
      break;
    case 'sport':
      message = `Received sport ${data.name} page ${data.meta.page}`;
      pages[data.meta.page].sports[data.name.toLowerCase()] = data;
      break;
    case 'game':
      player1 = data.players[0].name;
      player2 = data.players[1].name;
      message = `Received game ${player1} vs ${player2} (${data.meta.sport}) page ${data.meta.page}`;
      pages[data.meta.page].sports[data.meta.sport.toLowerCase()].games[data.id] = data;
      break;
    case 'odd':
      player1 = pages[data.meta.page].sports[data.meta.sport.toLowerCase()].games[data.meta.game].players[0].name;
      player2 = pages[data.meta.page].sports[data.meta.sport.toLowerCase()].games[data.meta.game].players[1].name;
      message = `\tReceived odd ${data.name} for game ${player1} vs ${player2} (${data.meta.sport}) - ${data.values.join(', ')} - page ${data.meta.page}`;

      player1 = player1.toLowerCase();
      player2 = player2.toLowerCase();

      let game = pages[data.meta.page].sports[data.meta.sport.toLowerCase()].games[data.meta.game];
      game.odds.push(data);
      let name = player1.localeCompare(player2) < 0 ? player1 : player2;

      if (!games[name]) {
        games[name] = Object.assign({}, game);
        games[name].odds = {};
      } else {
        games[name].odds[data.name+' ('+data.meta.page+')'] = data;
      }
      sendMessage(`added odd: ${name}: ${JSON.stringify(games[name])}<br>DATA: ${data}`);
      break;
    default:
      console.error(`Received DEFAULT: ${JSON.stringify(data)}`);
      break;
  }

  // sendMessage(message);
}

function scrape(page) {
  try {
    const spawn = require('child_process').spawn,
      ls = spawn('./node_modules/casperjs/bin/casperjs', [`./dist/bots/${page}.js`]);

    ls.on('message', (msg) => {
      this.echo(`ScrapER RECEIVED ${msg}`);
    });

    ls.stdout.on('data', (data) => {
      const buff = new Buffer(data),
        messages = buff.toString('utf8').split('\n');

      for (const message of messages) {
        if (!!message) {
          try {
            if (/Wait timeout/i.test(message)) {
              process.send({ type: 'finished', page: arguments[0] });
            } else {
              formatData(JSON.parse(data));
            }
          } catch (e) {
            process.send({ err_code: 422, message: e.message });
          }
        }
      }
      process.send({ pages, type: 'pages' });
    });

    ls.stderr.on('data', (data) => {
      const buff = new Buffer(data),

      message = buff.toString('utf8');
      sendData(JSON.parse({ err_code: 504, message }));
    });

    ls.on('close', (code) => {
      process.send({ games: getGames(pages), type: 'games' });
      process.send({ pages, type: 'pages' });
      sendMessage(`child process exited with code ${code}`);
      process.send({ type: 'finished', page: arguments[0] });
    });

  } catch (e) {
    console.log(`ERROR: ${e.message}`);
  }
}

process.on('message', (msg) => {
  if (!!msg.page) {
    scrape(msg.page);
  } else if (!!msg.games) {
    process.send({ games: getGames(pages), type: 'games' });
  } else if (!!msg.get) {
    process.send({ pages, type: 'pages' });
  }
});
